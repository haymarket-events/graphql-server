/*
  Generate MongoDB testing data for Haymarket Events
*/
use HaymarketEvent

// event
db.createCollection("events")
db.getCollection("events").insert(
  {
    id: "digitalmediaawards-china-2018",
    navLogo: { // :Photo
      w: 144,
      src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/2017/02/footer-logo-campaign.png"
    },
    year: 2018,
    name: "Digital Media Awards",
    url: "http://www.digitalmediaawards-china.com",
    startDate: new Date("2018-04-26T08:00:00Z"),
    endDate: new Date("2018-04-26T18:00:00Z"),

    navigations: [
      { // :Navigation
        item: { // :CallForAction
          url: "/",
          label: "Home"
        },
        sortorder: 1,
      },
      {
        item: {
          url: "#rules",
          label: "Guidelines"
        },
        sortorder: 2,
      },
      {
        item: {
          url: "#shortlist-winner",
          label: "Shortlists"
        },
        sortorder: 3,
      },
      {
        item: {
          url: "#shortlist-winner",
          label: "Winners"
        },
        sortorder: 4,
      },
      {
        item: {
          url: "#jury",
          label: "Jury"
        },
        sortorder: 5,
      },
      {
        item: {
          url: "#categories",
          label: "Categories"
        },
        sortorder: 6,
      },
      {
        item: {
          url: "#gallery",
          label: "Gallery"
        },
        sortorder: 7,
      },
      {
        item: {
          url: "#sponsors",
          label: "Sponsors"
        },
        sortorder: 8,
      },
      {
        item: {
          url: "#contact",
          label: "Contact Us"
        },
        sortorder: 9,
      },
      {
        item: {
          url: "https://www.wechat.com/en/",
          label: "WeChat",
          src: "fa-wechat"
        },
        sortorder: 9,
      },
      {
        item: {
          url: "https://www.linkedin.com/",
          label: "Linkedin",
          src: "fa-linkedin"
        },
        sortorder: 10,
      },
    ],
    sns: [
      { // :Navigation
        item: { // :CallForAction
          url: "https://www.wechat.com/en/",
          label: "WeChat",
          src: "fa-wechat"
        },
        sortorder: 1,
      },
      {
        item: {
          url: "https://www.linkedin.com/company/campaign-asia-pacific/",
          label: "Linkedin",
          src: "fa-linkedin"
        },
        sortorder: 2,
      },
      {
        item: {
          url: "https://www.facebook.com/CampaignAPAC/",
          label: "Facebook",
          src: "fa-facebook"
        },
        sortorder: 3,
      },
      {
        item: {
          url: "https://twitter.com/CampaignAsia",
          label: "Twitter",
          src: "fa-twitter"
        },
        sortorder: 4,
      },
      {
        item: {
          url: "https://www.youtube.com/campaignasia",
          label: "Youtube",
          src: "fa-youtube"
        },
        sortorder: 5,
      },
    ],
    jumbotron: {
      logo: { // :Photo
        w: 645,
        h: 326,
        src: "http://www.digitalmediaawards-china.com/wp-content/uploads/sites/3/2017/03/hero-logo.png",
      },
      background: { // :Photo
        w: 1920,
        h: 1244,
        src: "http://www.digitalmediaawards-china.com/wp-content/themes/digital-media-awards/images/hero-bg.png",
      },
      content: [
        "Entry Deadline : 13 March 2018",
        "Awards Presentation Date : 26 April 2018",
          "JW Marriott Hotel Shanghai at Tomorrow Square"
      ]
    },
    footer: {
      logo: { // :Photo
        w: 144,
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/2017/02/footer-logo-campaign.png"
      },
      content: [
        "Copyright © 2018 CampaignAsia, All Rights reserved"
      ]
    },

    intro: { // :Program
      speakers_as_People: ["atifa-silk"], // > db.people.id
      name: "The Digital Media Awards China 2018",
      body: [
        `The Digital Media Awards China 2018, organised by Campaign, are setting out to celebrate Greater China's best digital marketing work, innovation and talent, and recognise the region's growing influence on the global digital industry.
As China's digital industry continues to lead the world, this awards competition, judged to international standards by the industry's most respected digital professionals, aims to celebrate outstanding work and talent and, through the global reach of the Campaign brand, put the winners on the international stage
Categories have been selected to reflect the development and growth of digital marketing in China. As you'll find in this entry kit, work can be entered by vertical sector and by type of media engaged. In our effort to honor and nurture the best talent, these awards feature people categories to recognise the industry's digital leaders, as well as rising stars. In addition, Campaign will be handing out the Platinum Award to the best campaign and crowning the Digital Media Awards Agency of the Year 2018.
An independent jury, comprising leading client marketers, digital practitioners and agency strategists, as well as other experts in the field of digital marketing, will judge entries. Please note these awards are open to all Chinese-language work from the Greater China region.
We look forward to a truly competitive year and uncovering work that's innovative and inspirational.`,
        "Digital Media Awards China is organised by Haymarket Media Ltd."
      ]
      // time: null
    },

    // dynamic fields
    photos: [
      { //: Photo
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1000.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_0998.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1007.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1057.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1062.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1066.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1075.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1097.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1104.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1106.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1107.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1109.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1111.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1114.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1116.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1121.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1126.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1132.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1136.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1142.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1145.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1154.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1158.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1161.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1166.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1172.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1174.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1178.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1181.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1182.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1188.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1198.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1200.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1203.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1208.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1213.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1214.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1216.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1218.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1226.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1227.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1229.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1232.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1234.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1236.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1239.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1245.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1249.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1250.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1255.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1258.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1261.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1263.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1266.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1267.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1270.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1271.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1274.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1277.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1278.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1281.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1287.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1293.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1295.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1298.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1304.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1308.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1310.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1311.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1315.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1317.JPG"
      },
      {
        src: "http://www.digitalmediaawards-china.com/en/wp-content/uploads/sites/28/nggallery/2017-gallery/thumbs/thumbs_IMG_1328.JPG"
      },
    ],
    People: [ // > db.people.id
      "anne-rayner",
      "kaveri-khullar",
      "lizi-hamer",
      "michelle-wong",
      "pratik-thakar",
      "rose-huskey",
      "rupen-desai",
      "ruth-stubbs",
      "wendy-hogan",
      "atifa-silk",
    ],
    Contacts: [ // > db.contacts.id
      "zamir-khan",
      "eleanor-hawkins",
      "kenny-holroyd",
      "atifa-silk"
    ],
    partners: [
      { // :Partner
        title: "Headline Partners",
        Brands: [ // > db.brands.id
            "groupm",
        ]
      },
      {
        title: "Partners",
        Brands: [
            "edelman",
            "essenceglobal",
            "google",
            "mediacom",
            "mindshareworld",
            "wavemakerglobal",
        ]
      }
    ],
    programmes: [
      { //: Programme
        name: "Registration and pre-conference networking",
        startDate: new Date("2018-04-26T08:00:00Z"),
        endDate: new Date("2018-04-26T09:05:00Z"),
        description: "",
        heroes_as_People: [],
      },
      {
        name: "Campaign's opening remarks",
        startDate: new Date("2018-04-26T09:05:00Z"),
        endDate: new Date("2018-04-26T09:10:00Z"),
        description: "Campaign Asia-Pacific welcomes you to the second annual Campaign360; an event designed to rethink our future and accelerate growth for the business of media, through the power of technology and transformation.",
        heroes_as_People: [ // > db.preople.id
          "atifa-silk",
        ],
      },
      {
        name: "Through great disruption, comes great opportunity",
        startDate: new Date("2018-04-26T09:10:00Z"),
        endDate: new Date("2018-04-26T09:40:00Z"),
        description: "With multiple and complex forces impacting business models, 2018 is set to be a year of great change. How are industry leaders eyeing future growth? This opening keynote session sets the scene for Campaign360, bringing the big questions to the fore and exploring the **forces of growth** for the business of media in Asia and globally.",
        heroes_as_People: [
          "kaveri-khullar",
          "lizi-hamer",
        ],
      },
      {
        name: "Everything has changed, so why haven't we?",
        startDate: new Date("2018-04-26T09:40:00Z"),
        endDate: new Date("2018-04-26T10:15:00Z"),
        description: "The media environment is moving at great speed: Media formats, the tools, the technology, the talent. Media organisational structures and traditional business models will not stand the test of time and media companies must reinvent if they are to survive. If we don’t adapt at the speed of the consumer, do we run the risk of diminishing altogether? What does being nimble even mean? This panel discussion brings together leaders from every corner of the industry to find the solution and come up with a plan. Let’s collaborate and find a way to progress.",
        heroes_as_People: [
          "michelle-wong",
          "pratik-thakar",
          "rose-huskey",
        ],
      },
      {
        name: "Networking and morning refreshments",
        startDate: new Date("2018-04-26T10:15:00Z"),
        endDate: new Date("2018-04-26T10:55:00Z"),
        description: "",
        heroes_as_People: [],
      },
      {
        name: "Leading APAC into a brighter media future",
        startDate: new Date("2018-04-26T10:55:00Z"),
        endDate: new Date("2018-04-26T17:35:00Z"),
        description: `Regional CEOs are under immense pressure as the global macro challenges continue to affect APAC. Change requires new and innovative technologies, industry collaboration and the bravery to reinvent.
        In this session, we bring it back to APAC. What strategic changes do CEOs need to make in the next 12 months to fight away the disruptive forces that have damaged the business of media? What opportunities does APAC present and where are these pockets of growth? Do our regional CEOs feel positive about the future?`,
        heroes_as_People: [
          "lizi-hamer",
          "pratik-thakar",
          "ruth-stubbs",
          "michelle-wong",
          "rupen-desai",
          "rose-huskey",
        ],
      },
      {
        name: "The Campaign360 debrief",
        startDate: new Date("2018-04-26T17:35:00Z"),
        endDate: new Date("2018-04-26T17:55:00Z"),
        description: "We’ve debated, analysed, strategised and shared learnings but what are the real takeaways from the day? This closing speech digests the day.",
        heroes_as_People: [
          "atifa-silk",
        ],
      },
    ],
    guildlines: [
      { //: Section
        sortorder: 1,
        slug: "",
        callForAction: [],
        title: "Who Should Enter",
        description: "Entry is open to all clients, media owners, advertising agencies, digital agencies, production or design companies, or any other parties involved in digital communications for advertising purposes. Any entry for a particular project should be coordinated between the different parties involved.",
      },
      {
        sortorder: 2,
        slug: "",
        callForAction: [],
        title: "Eligibility Period",
        description: "All English or Chinese language entries and nominations will be accepted. All entries should relate to achievements ONLY during the period 1 January 2017 – 13 February 2018.\n\nWritten entries which do not focus on the eligibility period but include achievements outside the review period will be disqualified. Campaign reserves the right to reject work it feels does not comply with the spirit of the awards.",
      },
      {
        sortorder: 3,
        slug: "",
        callForAction: [],
        title: "Dates and Deadlines",
        description: "Early Bird Entry Deadline: Tuesday, 13 February 2018 (6PM HKT)\nEntry Deadline: Tuesday, 13 March 2018 (6PM HKT)\n\nShortlists Announcement: Wednesday, 4 April 2018\nAwards Presentation: Thursday, 26 April 2018 (Shanghai, China)",
      },
    ]
  }
)

// brand
db.createCollection("brands")
db.getCollection("brands").insert({ // :Brand
  id: "groupm",
  name: "Groupm",
  url: "https://www.groupm.com/",
  src: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/02/GroupM_Hero_Logo_RGB_png-e1519114456428.png",
});
db.getCollection("brands").insert({
  id: "edelman",
  name: "Edelman",
  url: "http://www.edelman.com/",
  src: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/02/Untitled-design-35.png",
});
db.getCollection("brands").insert({
  id: "essenceglobal",
  name: "Essenceglobal",
  url: "https://www.essenceglobal.com/",
  src: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/02/Untitled-design-30.png",
});
db.getCollection("brands").insert({
  id: "google",
  name: "Google",
  url: "https://www.google.com/about/?hl=en",
  src: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/02/Untitled-design-31.png",
});
db.getCollection("brands").insert({
  id: "mediacom",
  name: "Mediacom",
  url: "https://www.mediacom.com/hk",
  src: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/02/Untitled-design-33.png",
});
db.getCollection("brands").insert({
  id: "mindshareworld",
  name: "Mindshareworld",
  url: "http://www.mindshareworld.com/",
  src: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/02/Untitled-design-34.png",
});
db.getCollection("brands").insert({
  id: "wavemakerglobal",
  name: "Wavemakerglobal",
  url: "https://www.wavemakerglobal.com/",
  src: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/02/Untitled-design-29.png",
});


// contact
db.createCollection("contacts")
db.getCollection("contacts").insert({
    id: "zamir-khan",
    type: "",
    name: {
      firstName: "Zamir",
      lastName: "Khan",
    },
    jobTitle: "Head of Awards Events",
    email: "zamir.khan@haymarket.asia",
    phone: "+852 2122 5273",
    // address { streetAddress, city, postalCode, type, addressFormat, location { lat, lng, zoom }}
})
db.getCollection("contacts").insert({
    id: "eleanor-hawkins",
    type: "",
    name: {
      firstName: "Eleanor",
      lastName: "Hawkins",
    },
    jobTitle: "Senior Conference Producer",
    email: "eleanor.hawkins@haymarket.asia",
    phone: "+852 3175 1922",
    // address { streetAddress, city, postalCode, type, addressFormat, location { lat, lng, zoom }}
})
db.getCollection("contacts").insert({
    id: "atifa-silk",
    type: "",
    name: {
      firstName: "Atifa",
      lastName: "Silk",
    },
    jobTitle: "Brand Director",
    email: "atifa.silk@haymarket.asia",
    phone: "+852 3118 1500",
    // address { streetAddress, city, postalCode, type, addressFormat, location { lat, lng, zoom }}
})
db.getCollection("contacts").insert({
    id: "kenny-holroyd",
    type: "",
    name: {
      firstName: "Kenny",
      lastName: "Holroyd",
    },
    jobTitle: "Head of Sponsorship, Campaign Asia-Pacific",
    email: "kenny.holroyd@haymarket.asia",
    phone: "+65 6579 0556",
    // address { streetAddress, city, postalCode, type, addressFormat, location { lat, lng, zoom }}
})

// person
db.createCollection("people")
db.getCollection("people").insert({
    id: "anne-rayner",
    year: 2018,
    name: {
      firstName: "Anne",
      lastName: "Rayner"
    },
    jobTitle: "Global Head of Communications Research",
    organization: "Kantar TNS",
    bio: "Researcher, thought leader and advocate Anne Rayner leads the Kantar TNS Communication Research practice, advising on touchpoint optimisation, media evaluation, creative development and performance tracking. She is a firm believer in the value of Integrating digital and social data into brand and communications insights, and has been at the forefront of driving innovation in this area.\nAnne’s passion project is promoting gender equality in media and marketing, and championing diversity and inclusion. A ‘Global Women in Research Top Ten MRX Diversity Champion’, she is executive sponsor of Kantar TNS’s Australian Inclusion & Diversity steering committee and founding member of its global I&D committee.",
    avatar: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/03/Anne-Rayner.jpg"
})
db.getCollection("people").insert({
    id: "kaveri-khullar",
    year: 2018,
    name: {
      firstName: "Kaveri",
      lastName: "Khullar"
    },
    jobTitle: "Head of Marketing Innovation, Global Johnnie Walker Reserve",
    organization: "Diageo",
    bio: "Kaveri Khullar is a Marketing Consultant with Diageo South East Asia, a role she has taken on as she transitions to her next exciting opportunity that will be disclosed in a few weeks. In her last role, she was the Head of Marketing Innovation for Johnnie Walker Reserve, Diageo’s super - premium Scotch portfolio, responsible for delivering performance through sound go-to-market strategies, scalable route to consumer programs, digital consumer engagement and most critically, influencing stakeholders to drive key initiatives.\nPrior to her global role based out of Singapore, she led the charge of setting up Diageo's luxury vertical in India, delivering category leadership in a span of two years in Scotch and ultra-premium Vodka. Subsequently, she was handed the mandate of consolidating and turning around a highly fragmented international spirits portfolio to outperform competition in all key battlegrounds. Before she left India, it had become the strongest performing business unit for the market with a best practice growth model.\nFrom managing a wide range of international and domestic artists and leading B2B partnerships in her 7 years at EMI Virgin to working with the finest and most coveted spirits brands in the world, her trajectory has been anything but conventional. What has however remained constant over these 16 + years is her quest for new and challenging frontiers, her passion for delivering against stretch performance goals and the knack of failing fast to come back stronger. When not pushing limits in the boardroom, Kaveri is challenging herself outdoors. Her most recent endeavor was a successful summit of Mount Kilimanjaro and one day, she hopes to have a story to tell, of resilience and of embracing the crazy as she eyes the other six peaks.",
    avatar: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/03/Khullar-Kaveri.jpg"
})
db.getCollection("people").insert({
    id: "lizi-hamer",
    year: 2018,
    name: {
      firstName: "Lizi",
      lastName: "Hamer"
    },
    jobTitle: "Regional Creative Director",
    organization: "Octagon",
    bio: "Lizi Hamer is a Story Hunter with a passion to make the world better, and the enthusiasm to do it.",
    avatar: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/03/Lizi-Hamer.jpg"
})
db.getCollection("people").insert({
    id: "michelle-wong",
    year: 2018,
    name: {
      firstName: "Michelle",
      lastName: "Wong"
    },
    jobTitle: "Director, CRM &amp; Analytics (Global Marketing)",
    organization: "McDonald's APMEA",
    bio: "Michelle is part of McDonald's Global Media, CRM and Merchandising team that aims to unlock the full value of all McDonald's customers through data. Her vision is that “Today we serve close to 69 million customers in over 100 countries on a daily basis. Tomorrow, we want to have personal conversations with these customers”. Her team is focused on using the data they have on customers to create value, utility and meaningful experiences. We have the ability to say, ‘We remember you, we remember what you ordered, we know your preferences and can engage with you personally.’”\nPreviously, she spent 11 years at Ogilvy where she led a hybrid team of Customer Engagement Strategy, User Experience and Marketing Analytics. Outside of work, she is a struggling violinist and believes one can never have enough champagne.",
    avatar: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/03/Michelle-Wong.jpeg"
})
db.getCollection("people").insert({
    id: "pratik-thakar",
    year: 2018,
    name: {
      firstName: "Pratik",
      lastName: "Thakar"
    },
    jobTitle: "Group Director, Creative, Content &amp; Design Excellence",
    organization: "The Coca-Cola Company",
    bio: "Pratik Thakar leads the creative, content and design excellence across Coca-Cola Company’s ASEAN 10-plus markets. He leads various global charters and incubates innovative and creative marketing solutions for the entire brand portfolio of the company. He held leadership roles in many global ad agencies before joining Coke. His 18 years of industry experience covers stints in India, Southeast Asia and Greater China for Grey Group, McCann Worldgroup, Saatchi & Saatchi, DDB and Lowe. A strong advocate of creative technology and collaborative innovation, he founded Grey Innovation lab as chief strategy and innovation officer for Grey Group Greater China.\nBefore joining Grey, he was the chief strategy and innovation catalyst at McCann Worldgroup, where he was highly regarded for his integrated creative strategies and disruptive ideas in design, branded content and digital technology-led innovation on brands such as Puma, L’Oréal Group, MasterCard, Nestlé, Intel and Coca-Cola. He has also served in advisory roles for start-up accelerators throughout Asia Pacific, getting the front row seat to the most cutting-edge technology and innovations of tomorrow. This combination of veteran brand strategy, multi-regional experience, and ahead-of-the-curve innovation places him as a leading voice for creative technology and 10X approach of building high-tech start-ups.",
    avatar: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/03/PRATIK-THAKAR.jpg"
})
db.getCollection("people").insert({
    id: "rose-huskey",
    year: 2018,
    name: {
      firstName: "Rose",
      lastName: "Huskey"
    },
    jobTitle: "Managing Director, Global Client Solutions",
    organization: "Wavemaker Global ",
    bio: "Rose is a media professional with over twenty years of experience in local and international marketing. As the Managing Director of Global Client Solutions for the Wavemaker, she is responsible for managing and growing Global relationships with key clients across Asia Pacific, including; L’Oréal, Tiffany & Co, Chevron, Daimler, Jetstar, Netflix and FreslandCampina.\nWavemaker is a brand new billion-dollar agency created from the merger of two of GroupM’s leading global networks; MEC and Maxus, ranked #2 globally and #3 in APAC. The agency is focused on media, content and technology, with Rose working across all three verticals to provide clients with integrated solutions.\nPrior to Wavemaker, Rose was Chief Client Officer for Maxus Asia Pacific, Managing Director at Maxus Vietnam, and held positions at OMD, Teligence and MW Media in Vancouver, Canada.\nRose has a reputation for outstanding agency and account management, built on a foundation of close client relationships, meaningful team development, and savvy financial understanding.",
    avatar: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/03/Rose-Huskey.jpg"
})
db.getCollection("people").insert({
    id: "rupen-desai",
    year: 2018,
    name: {
      firstName: "Rupen",
      lastName: "Desai"
    },
    jobTitle: "Vice Chairman, Asia-Pacific, Middle East &amp; Africa",
    organization: "Edelman",
    bio: "Rupen leads Edelman’s integrated brand business across Asia Pacific, Middle East and Africa, covering the areas of digital, consumer PR, social media and brand communications.\nA strong advocate of the impact purpose-led business and creativity have on society as well as on profit, he brings 20 years of experience in leading multicultural teams that have shaped brand narratives for some of the world’s most famous, consistent and successful brands.\nPrior to Edelman, Rupen spent two decades working for Lowe + Partners, living across Asia, Africa, Middle East and Europe. He was the network’s Asia Pacific Regional President for six years, during which period the network became a top performer in new business leagues and effectiveness awards.\nRupen is also a board member of Female Founders, a not-for-profit organization dedicated to the pursuit of gender equality in entrepreneurship and leadership. This issue is of personal importance, as he believes in the economic and moral impact of getting this right for our future generations.",
    avatar: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/03/Rupen.jpg"
})
db.getCollection("people").insert({
    id: "ruth-stubbs",
    year: 2018,
    name: {
      firstName: "Ruth",
      lastName: "Stubbs"
    },
    jobTitle: "Global President",
    organization: "iProspect",
    bio: "As President, Ruth is responsible for iProspect’s global proposition, business growth strategy and the development of focused initiatives supported by Dentsu Aegis, with over 4,000 people in 52 markets around the world.\nRuth has over 25 years’ experience in Media and Digital Marketing. She joined iProspect APAC in 2011 and during her tenure supported the integration of seven acquisitions and achieved over 50% growth in revenue YoY. Ruth previously held leadership roles with Mediabrands and GroupM Interaction APAC. Ruth serves as executive sponsor of Female Foundry, a passion project designed to mentor and develop female-led start-ups in Asia.",
    avatar: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/03/Ruth-Stubbs.jpg"
})
db.getCollection("people").insert({
    id: "wendy-hogan",
    year: 2018,
    name: {
      firstName: "Wendy",
      lastName: "Hogan"
    },
    jobTitle: "Customer Experience &amp; Marketing Strategy Director",
    organization: "Oracle APAC",
    bio: "Wendy Hogan is Customer Experience and Marketing Strategy Director at Oracle APAC. In this role, Wendy is responsible for designing and delivering go-to-market and insights for business leaders looking to redefine how they engage with their customer and prospects. Prior to her tenure at Oracle, Wendy worked in media and advertising for the likes of CBS Interactive, ExchangeWire.com, mig.me and ContentAsia.TV.\nWendy is currently non-executive director for Media Beach and Visual Amplifiers and a board member of The Marketing Society SEA & I-Com Singapore Advisory Board. Wendy is founder of learning network SWIMMA (Singapore Women in Media, Marketing and Advertising) and co-founder of www.ladybadassery.com where she functions as the APAC curator.",
    avatar: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/03/Wendy-Hogan.jpeg"
})
db.getCollection("people").insert({
    id: "atifa-silk",
    year: 2018,
    name: {
      firstName: "Atifa",
      lastName: "Silk"
    },
    jobTitle: "Brand Director",
    organization: "Campaign Asia-Pacific",
    bio: "Atifa Silk has been covering the region’s marketing communications industry for 11 years, having joined Haymarket in 2000 as assistant editor on Media magazine. She became editor in 2006. In January 2011, she took on the role of editorial director for Haymarket’s Brand Media division, which includes Campaign Asia-Pacific and CEI, the region’s leading MICE magazine.\nAtifa oversees content for all of Campaign’s print and digital products, as well as face-to-face events in the region.",
    avatar: "http://womenleadingchangeawards.com/wp-content/uploads/sites/26/2018/02/Atifa-Silk-320x320.jpg"
})
