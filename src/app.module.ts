import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { GraphQLModule, GraphQLFactory } from '@nestjs/graphql';
import { MergeGraphQLSchema } from './infrastructures/graphql/mergeSchema';
import { mergeSchemas } from 'graphql-tools';
import { Module, NestModule, MiddlewaresConsumer, RequestMethod } from '@nestjs/common';
import { siteConstants, ConfigName } from './infrastructures/siteConstants';
// controllers
import { AppController } from './app.controller';

@Module({
  imports: [
    GraphQLModule,
  ],
  controllers: [AppController],
  components: [
    MergeGraphQLSchema,
  ],
})

export class ApplicationModule implements NestModule {
  constructor(
    private readonly graphQLFactory: GraphQLFactory,
    private readonly mergeGraphQLSchema: MergeGraphQLSchema,
  ){}

  configure(consumer: MiddlewaresConsumer) {
    const schema = this.constructSchema('./**/*.graphql');

    consumer
      .apply(
        graphqlExpress(req => ({
          schema,
          rootValue: req,
          tracing: true, // enable response caching
          cacheControl: true, // enable response caching
          }),
        ),
      )
      .forRoutes({ path: '/graphql', method: RequestMethod.ALL });

    if (siteConstants.env === ConfigName.debug) {
      consumer
      .apply(
        graphiqlExpress({
          endpointURL: '/graphql',
          subscriptionsEndpoint: `ws://localhost:3001/subscriptions`,
        }),
      )
      .forRoutes({ path: '/graphiql', method: RequestMethod.GET });
    }
  }

  private constructSchema(path: string) {
    const typeDefs = this.mergeGraphQLSchema.mergeTypesByPaths(path);
    const localSchema = this.graphQLFactory.createSchema({ typeDefs });
    const delegates = this.graphQLFactory.createDelegates();

    const schema = mergeSchemas({
      schemas: [localSchema],
      resolvers: delegates,
    });

    return schema;
  }
}
