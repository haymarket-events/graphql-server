import { mergeTypes } from 'merge-graphql-schemas';
import * as glob from 'glob';
import * as fs from 'fs';

/*
 * @nestjs/graphql use older version of merge-graphql-schemas, which have problem on schema merging
 * see https://github.com/okgrow/merge-graphql-schemas/issues/113
*/
export class MergeGraphQLSchema {
  mergeTypesByPaths(...pathsToTypes) {
    return mergeTypes(...pathsToTypes.map(pattern => this.loadFiles(pattern)), { all: true });
  }

  private loadFiles(pattern) {
    const paths = glob.sync(pattern);
    return paths.map(path => fs.readFileSync(path, 'utf8'));
  }
}
