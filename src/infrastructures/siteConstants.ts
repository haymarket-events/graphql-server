const env = process.env.NODE_ENV;

export enum ConfigName {
  debug,
  live,
}
class Constants {
  env: ConfigName;
  databaseConnectionName: string;
  databaseConnectionString: string;
}

class SiteConstants {
  debug: Constants;
  live: Constants;
}

const debugConfig: Constants = {
  env: ConfigName.debug,
  databaseConnectionName: 'MongoDBConnection',
  // tslint:disable-next-line:max-line-length
  databaseConnectionString: 'mongodb://mongodb_user:mongodb_user@cluster0-shard-00-00-hdods.mongodb.net:27017,cluster0-shard-00-01-hdods.mongodb.net:27017,cluster0-shard-00-02-hdods.mongodb.net:27017/HaymarketEvent?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin',
};

const liveConfig: Constants = {
  env: ConfigName.live,
  databaseConnectionName: '',
  databaseConnectionString: '',
};

const constants: SiteConstants = {
  debug: debugConfig,
  live: liveConfig,
};

export const siteConstants = constants.debug; // [env];
