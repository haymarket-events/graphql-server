import * as mongoose from 'mongoose';
import { siteConstants } from '../siteConstants';

const mongodbProvider = {
    provide: siteConstants.databaseConnectionName,
    useFactory: async (): Promise<mongoose.Mongoose> => {
        (mongoose as any).Promise = global.Promise;
        return await mongoose.connect(siteConstants.databaseConnectionString)
    }
}

export const databaseProviders = [
    mongodbProvider,
]
