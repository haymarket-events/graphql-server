# Repository for Haymarket Events

## setup
open project folder dir in terminal, run
```
npm install
```

---


## start project (using terminal)
### start
``` sh
npm run start
```

### start w/ nodemon
```
npm run start:watch
```
(type rs to force reload nodemon)
### Typescript transpile
```
npm run prestart:prod
```
### run built code
```
npm run prestart:prod
npm run start:prod
```

---

## start project (using VSCode)
### start Debugging
1. Open **Debug** panel (`Ctrl+Shift+D`)
2. Choose **Launch.Debug** profile
3. Start Debugging (`F5`)

---
### Typescript transpile
1. `Ctrl+Shif+P`
2. `Task: Run Task`
3. choose `ts.compile`

(Quick way: `Ctrl+Shift+B`)

---
### compile and run
1. Open **Debug** panel (`Ctrl+Shift+D`)
2. Choose **Launch.Transpiled** profile
3. Start Debugging (`F5`)

---
### start with nodemon
```
npm run start:watch
```
1. Open **Debug** panel (`Ctrl+Shift+D`)
2. Choose **Attach (Inspector Protocol)** profile
3. Start Debugging (`F5`)

---
